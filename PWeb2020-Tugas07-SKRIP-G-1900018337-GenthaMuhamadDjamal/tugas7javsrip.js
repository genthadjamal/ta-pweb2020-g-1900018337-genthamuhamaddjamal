		function tekan()
		{
			var nama = (document.fform.inama.value);
			var nim = (document.fform.inim.value);
			var jurusan = (document.fform.ijurusan.value);
			var angkatan = (document.fform.iangkatan.value);


			var quis	= parseFloat(document.fform.iquis.value);
			var tugas	= parseFloat(document.fform.itugas.value);
			var uts		= parseFloat(document.fform.iuts.value);
			var uas		= parseFloat(document.fform.iuas.value);
			var ip 		= 0.0;
			var ket		= 0.0;

			var na = (0.10*quis)+(0.20*tugas)+(0.30*uts)+(0.40*uas);

			if((na>80) && (na<=100))
			{
				ip="A";
				ket = "Lulus dengan sangat baik";
			}
			else if ((na>69) && (na<=80))
			{
				ip="B";
				ket = "Lulus dengan baik";
			}
			else if ((na>55) && (na<=69))
			{
				ip="C";
				ket="Lulus dengan cukup";
			}
			else if ((na > 38) && (na<=55))
			{
				ip="D";
				ket="Lulus dengan kurang";
			}
			else
			{
				ip="E";
				ket="Tidak Lulus";
			}

			document.fform.oipk.value = ip;
			document.fform.oket.value = ket;

			document.fform.onama.value = nama;
			document.fform.onim.value = nim;
			document.fform.ojurusan.value = jurusan;
			document.fform.oangkatan.value = angkatan;
		}