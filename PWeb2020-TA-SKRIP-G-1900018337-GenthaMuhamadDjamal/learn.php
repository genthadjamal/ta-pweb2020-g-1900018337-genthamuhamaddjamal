<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Learn</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<section class="game">
	<div class="container">
		<div class="row">
			<div class="section-title text-center">
				<h1>PHP ARRAY</h1>
			</div>
		</div>
		<div class="row">
			<div class="game-form">
				<div class="row">
					<div class="text">
						<?php
						echo '<h2>Are you bored?</h2>';
						echo '<p>if you are bored try this who knows will cheer up</p>';
						?>
					</div>
				</div>

			<form method="post">
				<div class="row space-between">
					<div class="col-6">
						<input type="checkbox" class="form-control" name="a[]" value="8">8
                    </div>
                    <div class="col-6">
						<input type="checkbox" class="form-control" name="a[]" value="4">4
                    </div>
                    <div class="col-6">
						<input type="checkbox" class="form-control" name="a[]" value="6">6
					</div>

				</div>
				<div class="row">
				<div class="tekan">
					<input type="submit" class="kumpul">
				</div>
			</div>
			<br>
			</form>
				<?php
				if(isset($_POST['a'])){
					echo "<pre>";
					print_r($_POST['a']);
					echo "</pre>";
				}
				?>
			<br>
			<br>
			<div class="row">
				<div class="tekan">
					<?php
					echo '<a href="index.php">Delete</a>';
					?>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="game">
	<div class="container">
		<div class="row">
			<div class="section-title text-center">
				<h1>PHP FUNCTION</h1>
			</div>
		</div>
		<div class="row">
			<div class="game-form">
				<div class="row">
					<div class="text">
						<?php
						echo '<p>Luas segitiga menggunakan fungsi</p>';
						?>
					</div>
				</div>
				<?php
				function luas_segitiga($alas,$tinggi)
				{
					$luas = ($alas)/2 * $tinggi;
					return $luas;
				}
				echo "Luas Segitiga dengan alas 4 dan tinggi 15 = ".luas_segitiga(4,15);
				echo "<br>";
				echo "Luas Segitiga dengan alas 4 dan tinggi 7 = ".luas_segitiga(4,7);
				?>
			<br>
			<br>
			<div class="row">
				<div class="tekan">
					<?php
					echo '<a href="index.php">Halaman Awal</a>';
					?>
				</div>
			</div>
		</div>
	</div>
</section>
</body>
</html>