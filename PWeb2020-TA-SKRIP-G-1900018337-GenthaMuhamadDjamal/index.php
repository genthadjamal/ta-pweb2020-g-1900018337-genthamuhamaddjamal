<!DOCTYPE html>
<html>
<head>
	<title>WEB PROFILE</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<header>
	<div class="container">
		<div class="row">
		<div class="brand-name">
			<?php
			echo '<a href="" class="logo">gnthdjml</a>';
			?>
		</div>
		<div class="navbar">
			<ul>
				<li><a href="#home" class="active">Home</a></li>
				<li><a href="#about">About</a></li>
				<li><a href="#skill">Skill</a></li>
				<li><a href="#edu">Education</a></li>
				<li><a href="learn.php">Learn</a></li>
				<li><a href="#contact">Contact</a></li>
			</ul>
		</div>
	</div>
	</div>
</header>

<section class="home" id="home">
	<div class="container">
		<div class="row full-screen">
				<div class="home-content">
					<div class="block">
					<?php
					echo '<h6>Hello, My Name is</h6>';
					echo '<h1 class="gentha">Gentha Djamal</h1>';
					echo '<h3>Web Developer</h3>';
					echo '<div class="cv-btn">
							<a href="https://drive.google.com/file/d/1eVcgn_zTXxuVXlHSwpSvK1YKKAsoOcMf/view?usp=sharing">Download CV</a>
						  </div>';
					?>
				</div>	
			</div>
		</div>
	</div>
</section>

<section class="about-me" id="about">
	<div class="container">
		<div class="row">
			<div class="section-title">
				<h1>About me</h1>
				<p class="small text-uppercase">Information About Me</p>
			</div>
		</div>
		 <div class="row">
		 	<div class="about-content">
		 	  <div class="row">
		 		<div class="img">
		 			<img src="avatar.png" alt="about-me" width="20%">
		 		</div>
		 		<div class="text">
		 			<?php
		 			echo "<h4>I'M Gentha Djamal</h4>";
		 			echo "<h6>Student Informatics Engineering Ahmad Dahlan University</h6>";
		 			?>
		 			<div class="info">
		 			   <div class="list">
		 			   	<?php
		 				 echo "<label>Birthday:</label>";
		 				 echo "<p>23th May 2001</p>";
		 				?>
		 			   </div>
		 			   <div class="list">
		 			   	<?php
		 				  echo "<label>Email:</label>";
		 				  echo "<p> genthadjamal@gmail.com</p>";
		 				 ?>
		 			   </div>
		 			   <div class="list">
		 			   	<?php
		 				 echo "<label>Age:</label>";
		 				 echo "<p> 19 y.o</p>";
		 				 ?>
		 			   </div>
		 			   <div class="list">
		 			   	<?php
		 				 echo "<label>Phone:</label>";
		 				 echo "<p> 081578758364</p>";
		 				 ?>
		 			   </div>
		 			   <div class="list">
		 			   	<?php
		 				 echo "<label>Residence:</label>";
		 				 echo "<p> Indonesia</p>";
		 				 ?>
		 			   </div>
		 			   <div class="list">
		 			   	<?php
		 				 echo "<label>Address:</label>";
		 				 echo "<p>Karangsambung, Kebumen, Jawa Tengah</p>";
		 				?>
		 			   </div>
		 			</div>
		 		</div>
		 	</div>
		 	</div>
		 </div>
	</div>
</section>

<section class="skills" id="skill">
	<div class="container">
		<div class="row align-items-center">
			<div class="skills-content">
				<div class="row">
					<div class="section-title">
						<?php
						echo "<h1>Professional Skills</h1>";
						echo "<p>below is a skill that I can master so far and will continue to learn to develop it</p>";
						?>
					</div>
				</div>
				<div class="row">
					<div class="skill-box">
						<h6>C++</h6>
						<div class="skill-bar">
							<div class="skill-bar-in" style="width: 80%">
								<label>80%</label>
							</div>
						</div>
					</div>
					<div class="skill-box">
						<?php
						echo "<h6>Microsoft Office</h6>";
						?>
						<div class="skill-bar">
							<div class="skill-bar-in" style="width: 75%">
								<label>75%</label>
							</div>
						</div>
					</div>
					<div class="skill-box">
						<?php
						echo "<h6>Graphic Design</h6>";
						?>
						<div class="skill-bar">
							<div class="skill-bar-in" style="width: 60%">
								<label>60%</label>
							</div>
						</div>
					</div>
					<div class="skill-box">
						<?php
						echo "<h6>HTML5</h6>";
						?>
						<div class="skill-bar" >
							<div class="skill-bar-in" style="width: 80%">
								<label>80%</label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="img">
				<img src="data.png" width="40%">
			</div>
		</div>
	</div>
</section>

<section class="edu" id="edu">
	<div class="container">
		<div class="row">
			<div class="section-title">
				<?php
				echo "<h1>Education</h1>";
				?>
				<p class="small text-uppercase">My Educational History</p>
			</div>
		</div>
		 <div class="row">
		 	<div class="about-content">
		 	  <div class="row">
		 		<div class="img">
		 			<img src="school.png" alt="about-me" width="20%">
		 		</div>
		 		<div class="text">
		 			<div class="info">
		 			   <div class="list">
		 			   	<?php
		 				 echo "<label>2007-2013:</label>";
		 				 echo "<p>Elementary School 2 Karangsambung</p>";
		 				 ?>
		 			   </div>
		 			   <div class="list">
		 			   	<?php
		 				 echo "<label>2013-2016:</label>";
		 				 echo "<p>Junior High School 1 Karangsambung</p>";
		 				 ?>
		 			   </div>
		 			   <div class="list">
		 			   	<?php
		 				 echo "<label>2016-2019:</label>";
		 				 echo "<p>Senior High School 2 Kebumen</p>"
		 				 ?>
		 			   </div>
		 			   <div class="list">
		 			   	<?php
		 				  echo "<label>2019 - Now:</label>";
		 				  echo "<p>Ahmad Dahlan University</p>";
		 				 ?>
		 			   </div>
		 			</div>
		 		</div>
		 	</div>
		 	</div>
		 </div>
	</div>
</section>


<section class="contact-us">
	<div class="container">
		<div class="row">
			<div class="section-title text-center">
				<h1>Comment</h1>
			</div>
		</div>
		<div class="row">
			<div class="contact-form">
				<div class="row">
					<div class="text">
						<?php
						echo '<h2>Drop us a line</h2>';
						echo '<p>if you are going to comment about this website</p>';
						?>
					</div>
				</div>
				<form method="post" action="comment.php">
				<div class="row space-between">
					<div class="col-6">
						<input type="text" class="form-control" name="username" placeholder="Name">
					</div>
					<div class="col-6">
						<input type="text" class="form-control" name="email" placeholder="Email">
					</div>
				</div>
				<div class="row">
					<div class="col-12">
					<textarea class="form-control" placeholder="Your Comment" name="komen"></textarea>	
				</div>
			</div>
			<div class="row">
				<div class="tekan">
					<input type="submit" name="kirim" value="Kirim" class="btn">
					<input type="reset" name="kirim" value="Reset" class="btn">
				</div>
			</div>
		</form>
		</div>
	</div>
</section>


<section class="game">
	<div class="container">
		<div class="row">
			<div class="section-title text-center">
				<h1>Game</h1>
			</div>
		</div>
		<div class="row">
			<div class="game-form">
				<div class="row">
					<div class="text">
						<?php
						echo '<h2>Are you bored?</h2>';
						echo '<p>if you are bored try this who knows will cheer up</p>';
						?>
					</div>
				</div>

			<form action="" method="get">
				<div class="row space-between">
					<div class="col-6">
						<input type="text" class="form-control" name="name" id="name" placeholder="input your name in here">
					</div>

				</div>
				<div class="row">
				<div class="tekan">
					<input type="submit" class="kumpul">
				</div>
			</div>
			<br>
			</form>
				<?php
					if (isset($_GET['name'])) {
						$nama = $_GET['name'];
						for ($i=1; $i<=10; $i++){
						echo $nama." ";
							}
					}
				?>
			<br>
			<br>
			<div class="row">
				<div class="tekan">
					<?php
					echo '<a href="index.php">Delete</a>';
					?>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="footer" id="contact">
	<div class="footer-logo">
	<a href="index.php" style="text-decoration: none"><h1>GNTHDJML</h1></a>
	</div>
	<div class="footer-time">
		<p>
		<?php
			$hitung = 'counter.txt';
 
			function counter(){
			global $hitung;
 
			if(file_exists($hitung)){
				$value = file_get_contents($hitung);
			}else{
				$value = 0;
			}
			file_put_contents($hitung, ++$value);
			}
 			counter();
 
			echo 'Anda pengunjung ke: '.file_get_contents($hitung);
		?>
		<br>
			<?php
				date_default_timezone_set('Asia/Jakarta');
			 	echo date("l, d-m-Y H:i:s");
			?>
		</p>
	</div>
	<div class="footer-sosmed">
	<a href="https://web.facebook.com/profile.php?id=100004993770762" style="text-decoration: none"><img src="facebook.png" width="3%">&nbsp;</a>
	<a href="genthadjamal@gmail.com" style="text-decoration: none"><img src="gmail.png" width="3%">&nbsp;</a>
	<a href="https://instagram.com/gnthadjmal_" style="text-decoration: none"><img src="instagram.png" width="3%">&nbsp;</a>

	</div>

	<div class="footer-text">
	<p style="font-size: 15px; padding-top: 30px;">copyright 2020 - Gentha Muhamad Djamal</p>
	</div>
</div>

<script src="inijs.js"></script>
</body>
</html>
